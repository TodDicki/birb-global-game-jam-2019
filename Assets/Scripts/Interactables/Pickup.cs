﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    [HideInInspector] public Rigidbody rb;

    [SerializeField]
    private float weight;
    [SerializeField]
    private Transform holdPosition;
    [SerializeField]
    public BoxCollider[] bc;
    [SerializeField]
    private MeshCollider mc;

    private TrashCollecter player;
    private Nest nest;

    public bool canBePickedUp = true;
    private bool addWeight = true;

    public float Weight
    {
        get { return weight; }
        set { weight = value; }
    }

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        nest = FindObjectOfType<Nest>();
    }

    void OnTriggerEnter(Collider other)
    {

        Nest n = other.GetComponent<Nest>();
        if (n == null)
            n = other.GetComponentInParent<Nest>();
        if (n != null)
        {
            if (GetComponent<Rigidbody>().isKinematic)
                return;
            transform.parent = n.transform;
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Rigidbody>().freezeRotation = true;
            GetComponent<Rigidbody>().useGravity = false;
            enabled = false;
            FindObjectOfType<TrashCollecter>().currentWeight -= Weight;
            n.NestWeight += Weight;
            n.UpdateWeight(FindObjectOfType<TrashCollecter>());
            FindObjectOfType<TrashCollecter>().currentObject = null;
            for (int i = 0; i < bc.Length; i++)
            {
                bc[i].isTrigger = false;
            }
            Destroy(this);
        }
        //if (canBePickedUp)
        //{
        //    player = other.GetComponent<TrashCollecter>();
        //    if (player != null)
        //    {
        //        player.CurrentWeight += Weight;

        //        if (player.CurrentWeight <= player.HoldWeight)
        //        {
        //            if (player.canHoldMore)
        //            {
        //                if (player.HoldWeight >= Weight)
        //                {
        //                    bc[0].enabled = false;
        //                    Vector3 offset = holdPosition.position - transform.position;
        //                    transform.position = player.holdPosition.position - offset;
        //                    //tf.rotation = player.holdPosition.rotation;      // Might use this
        //                    transform.parent = player.holdPosition;
        //                    rb.isKinematic = true;
        //                    player.currentObject = this;
        //                }
        //            }
        //            else
        //            {
        //                Debug.Log("I Can't Pick That Up");
        //                player.CurrentWeight -= Weight;
        //            }
        //        }
        //        else
        //        {
        //            player.canHoldMore = true;
        //            player.CurrentWeight -= Weight;
        //        }
        //    }
        //}
        ////nest = other.GetComponent<Nest>();
        ////if (nest == null)
        ////    nest = other.GetComponentInParent<Nest>();
        //if (other.gameObject.tag == "Nest")
        //{
        //    if (addWeight)
        //    {
        //        nest.NestWeight += Weight;
        //        player.CurrentWeight -= Weight;
        //        addWeight = false;
        //        rb.isKinematic = true;
        //        gameObject.tag = "Nest";
        //        transform.parent = nest.transform;
        //        player.canHoldMore = true;
        //        if (bc != null)
        //        {
        //            bc[0].enabled = false;
        //            bc[1].enabled = false;
        //        }
        //        if (mc != null)
        //        {
        //            mc.enabled = false;
        //        }
        //        this.enabled = false;
        //    }
        //}
    }

    private void OnDestroy()
    {
        GetComponent<ShaderCollection>().RestoreShaders();
    }
}
