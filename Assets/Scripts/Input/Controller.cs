﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class InputDictionary : SerializableDictionary<Controller.ControllerType, InputScheme>
{ }

public class Controller : MonoBehaviour
{

    #region Enums and Classes
    public enum HeldState
    {
        Pressed,
        Held,
        Released
    }

    public enum Button
    {
        A,
        B,
        X,
        Y,
        LeftBumper,
        RightBumper,
        Back,
        Start,
        LeftClick,
        RightClick
    }

    public enum Axis
    {
        MovementHorizontal,
        MovementVertical,
        CameraHorizontal,
        CameraVertical,
        DPadHorizontal,
        DPadVertical,
        LeftTrigger,
        RightTrigger
    }

    public enum ControllerType
    {
        Keyboard,
        Xbox,
        PS4
    }

    private class ButtonEvent
    {
        public KeyCode button;
        public Action pressed = null;
        public Action held = null;
        public Action released = null;
    }

    private class AxisEvent
    {
        public string axis;
        public Action<float> action = null;
    }

    private class InputEvents
    {
        public Dictionary<Button, ButtonEvent> buttonEvents = new Dictionary<Button, ButtonEvent>();
        public Dictionary<Axis, AxisEvent> axisEvents = new Dictionary<Axis, AxisEvent>();
    }
    #endregion

    [SerializeField]
    private InputDictionary inputTypes = new InputDictionary();
    [SerializeField]
    private ControllerType currentControllerType = ControllerType.Xbox;
    [SerializeField, Tooltip("Time to check for connected and disconnected controllers.")]
    private float controllerCheckTimer = 1.5f;

    private Dictionary<ControllerType, InputEvents> inputEvents = new Dictionary<ControllerType, InputEvents>();

    private List<ControllerType> availableControllerTypes = new List<ControllerType>();

    public Action<ControllerType> onControllerConnected = null;
    public Action<ControllerType> onControllerDisconnected = null;

    private void Awake()
    {
        foreach (var inputType in Enum.GetValues(typeof(ControllerType)))
        {
            InputEvents events = new InputEvents();
            int buttonIndex = 0;
            int axisIndex = 0;
            foreach (var property in inputTypes[(ControllerType)inputType].GetType().GetFields())
            {
                if (property.FieldType == typeof(KeyCode))
                {
                    ButtonEvent buttonEvent = new ButtonEvent();
                    buttonEvent.button = (KeyCode)property.GetValue(inputTypes[(ControllerType)inputType]);
                    events.buttonEvents.Add((Button)buttonIndex, buttonEvent);
                    buttonIndex++;
                }
                if (property.FieldType == typeof(string))
                {
                    AxisEvent axisEvent = new AxisEvent();
                    axisEvent.axis = (string)property.GetValue(inputTypes[(ControllerType)inputType]);
                    events.axisEvents.Add((Axis)axisIndex, axisEvent);
                    axisIndex++;
                }
            }
            inputEvents.Add((ControllerType)inputType, events);
        }
    }

    private void Start()
    {
        availableControllerTypes = GetAvailableControllers();
        currentControllerType = availableControllerTypes[0];

        StartCoroutine(ControllerCheckRoutine());
    }

    public void RegisterButtonEvent(Button button, HeldState heldState, Action action)
    {
        foreach (var inputType in Enum.GetValues(typeof(ControllerType)))
        {
            if (heldState == HeldState.Held)
            {
                inputEvents[(ControllerType)inputType].buttonEvents[button].held += action;
            }
            else if (heldState == HeldState.Pressed)
            {
                inputEvents[(ControllerType)inputType].buttonEvents[button].pressed += action;

            }
            else if (heldState == HeldState.Released)
            {
                inputEvents[(ControllerType)inputType].buttonEvents[button].released += action;
            }
        }
    }

    public void UnregisterButtonEvent(Button button, HeldState heldState, Action action)
    {
        foreach (var inputType in Enum.GetValues(typeof(ControllerType)))
        {
            if (heldState == HeldState.Held)
            {
                inputEvents[(ControllerType)inputType].buttonEvents[button].held -= action;
            }
            else if (heldState == HeldState.Pressed)
            {
                inputEvents[(ControllerType)inputType].buttonEvents[button].pressed -= action;
            }
            else if (heldState == HeldState.Released)
            {
                inputEvents[(ControllerType)inputType].buttonEvents[button].released -= action;
            }
        }
    }

    public void RegisterAxisEvent(Axis axis, Action<float> action)
    {
        foreach (var inputType in Enum.GetValues(typeof(ControllerType)))
        {
            inputEvents[(ControllerType)inputType].axisEvents[axis].action += action;
        }
    }

    public void UnregisterAxisEvent(Axis axis, Action<float> action)
    {
        foreach (var inputType in Enum.GetValues(typeof(ControllerType)))
        {
            inputEvents[(ControllerType)inputType].axisEvents[axis].action -= action;
        }
    }

    private void Update()
    {
        ProcessInput();
    }

    void ProcessInput()
    {
        foreach (ButtonEvent buttonEvent in inputEvents[currentControllerType].buttonEvents.Values)
        {
            if (Input.GetKeyDown(buttonEvent.button))
            {
                if (buttonEvent.pressed != null)
                {
                    Debug.Log(9);
                    buttonEvent.pressed.Invoke();
                }
                //Debug.Log(buttonEvent.pressed.GetInvocationList().Length.ToString());
            }
            if (Input.GetKey(buttonEvent.button))
                if (buttonEvent.held != null)
                    buttonEvent.held.Invoke();
            if (Input.GetKeyUp(buttonEvent.button))
                if (buttonEvent.released != null)
                    buttonEvent.released.Invoke();
        }

        foreach (AxisEvent axisEvent in inputEvents[currentControllerType].axisEvents.Values)
        {
            try
            {
                if (axisEvent.action != null)
                    axisEvent.action.Invoke(Input.GetAxisRaw(axisEvent.axis));
            }
            catch (Exception e)
            {
                Debug.Log(axisEvent.axis);
            }
        }
    }

    private List<ControllerType> GetAvailableControllers()
    {
        List<ControllerType> ret = new List<ControllerType>();
        for (int i = 0; i < Input.GetJoystickNames().Length; i++)
        {
            string currentController = Input.GetJoystickNames()[i].ToLower();
            if (currentController.Contains("xbox"))// == "controller (xbox 360 for windows)" ||
                                                   //currentController == "controller (xbox 360 wireless for windows)" ||
                                                   //currentController == "controller (xbox one for windows)"))
            {
                ret.Add(ControllerType.Xbox);
            }
            if (currentController == "wireless controller")
            {
                ret.Add(ControllerType.PS4);
            }
        }
        ret.Add(ControllerType.Keyboard);
        return ret;
    }

    private IEnumerator ControllerCheckRoutine()
    {
        List<ControllerType> currentConnectedControllers;
        List<ControllerType> disconnectedControllers = new List<ControllerType>();
        List<ControllerType> newlyConnectedControllers = new List<ControllerType>();
        while (true)
        {
            yield return new WaitForSeconds(controllerCheckTimer);
            currentConnectedControllers = GetAvailableControllers();

            //Check for disconnections
            disconnectedControllers.AddRange(availableControllerTypes.Except(currentConnectedControllers));
            if (onControllerDisconnected != null)
            {
                for (int i = 0; i < disconnectedControllers.Count; i++)
                {
                    onControllerDisconnected.Invoke(disconnectedControllers[i]);
                }
            }

            //Check for new connections
            newlyConnectedControllers.AddRange(currentConnectedControllers.Except(availableControllerTypes));
            if (onControllerConnected != null)
            {
                for (int i = 0; i < newlyConnectedControllers.Count; i++)
                {
                    onControllerDisconnected.Invoke(newlyConnectedControllers[i]);
                }
            }

            availableControllerTypes = currentConnectedControllers;
            disconnectedControllers.Clear();
            newlyConnectedControllers.Clear();
        }
    }
}
