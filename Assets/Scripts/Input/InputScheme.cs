﻿using UnityEngine;

[CreateAssetMenu(menuName ="InputScheme")]
public class InputScheme : ScriptableObject {
    public string horizontalMovementAxis;
    public string verticalMovementAxis;
    public string horizontalCameraAxis;
    public string verticalCameraAxis;
    public string dPadXAxis;
    public string dPadYAxis;
    public string leftTriggerAxis;
    public string rightTriggerAxis;
    [Tooltip("The 'A' Button on Xbox. The 'X' button on Playstation.")]
    public KeyCode aButton = KeyCode.Joystick1Button0;
    [Tooltip("The 'B' Button on Xbox. The 'O' button on Playstation.")]
    public KeyCode bButton = KeyCode.Joystick1Button1;
    [Tooltip("The 'X' Button on Xbox. The Square button on Playstation.")]
    public KeyCode xButton = KeyCode.Joystick1Button2;
    [Tooltip("The 'Y' Button on Xbox. The Triangle button on Playstation.")]
    public KeyCode yButton = KeyCode.Joystick1Button3;
    [Tooltip("The Left Bumper | L1")]
    public KeyCode lBumper = KeyCode.Joystick1Button4;
    [Tooltip("The Right Bumper | R1")]
    public KeyCode rBumper = KeyCode.Joystick1Button5;
    [Tooltip("The Back Button on Xbox. The Share button on Playstation.")]
    public KeyCode backButton = KeyCode.Joystick1Button6;
    [Tooltip("The Start Button on Xbox. The Options button on Playstation.")]
    public KeyCode startButton = KeyCode.Joystick1Button7;
    [Tooltip("Left Stick Click on Xbox. L3 on Playstation.")]
    public KeyCode leftClick = KeyCode.Joystick1Button8;
    [Tooltip("Right Stick Click on Xbox. R3 on Playstation.")]
    public KeyCode rightClick = KeyCode.Joystick1Button9;
}
