﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneStart : MonoBehaviour {
    [SerializeField]
    private CameraController cameraPrefab = null;
    [SerializeField]
    private Transform startPoint = null;

	// Use this for initialization
	void Start () {
        SceneDataHolder sceneData = FindObjectOfType<SceneDataHolder>();
        CameraController cam = Instantiate(cameraPrefab);
        PlayerController bird = Instantiate(sceneData.characters[sceneData.characterIndex].prefab, startPoint.position, startPoint.rotation);
        cam.SetTarget(bird.transform);
	}

}
