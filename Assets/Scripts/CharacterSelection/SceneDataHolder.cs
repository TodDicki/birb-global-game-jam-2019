﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneDataHolder : MonoBehaviour {

    public CharacterData[] characters = null;
    public int characterIndex = 0;

    private void Awake()
    {
        if (FindObjectsOfType<SceneDataHolder>().Length > 1)
            Destroy(gameObject);
        else
            DontDestroyOnLoad(gameObject);
    }
}
