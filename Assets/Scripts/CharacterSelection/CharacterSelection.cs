﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour {

    [SerializeField]
    private CharacterData[] characters = null;

    [SerializeField]
    private Transform displayPosition = null;
    [SerializeField]
    private Image img = null;
    [SerializeField]
    private Text text = null;

    private GameObject displayObject = null;

    private SceneDataHolder sceneData = null;

    private Animator anim;

    private bool canCycle = true;

    private void Start()
    {
        sceneData = FindObjectOfType<SceneDataHolder>();

        ChangeDisplay();

        Controller controller = FindObjectOfType<Controller>();
        controller.RegisterAxisEvent(Controller.Axis.MovementHorizontal, CycleDirection);
        controller.RegisterButtonEvent(Controller.Button.A, Controller.HeldState.Pressed, () => StartCoroutine(FlyAway())); //UnityEngine.SceneManagement.SceneManager.LoadScene(2));
    }

    void CycleDirection(float direction)
    {
        int cycleDir = 0;
        if (direction > .5f)
            cycleDir = 1;
        else if (direction < -.5f)
            cycleDir = -1;
        else
            canCycle = true;

        if (canCycle && cycleDir != 0)
        {
            int index = sceneData.characterIndex;
            index += cycleDir;
            if (index < 0)
                index = sceneData.characters.Length - 1;
            if (index >= sceneData.characters.Length)
                index = 0;
            sceneData.characterIndex = index;

            ChangeDisplay();

            canCycle = false;
        }
    }

    void ChangeDisplay()
    {
        if(displayObject)
            Destroy(displayObject);
        displayObject = Instantiate(sceneData.characters[sceneData.characterIndex].visualPrefab, displayPosition.position, displayPosition.rotation);
        img.color = sceneData.characters[sceneData.characterIndex].uiShade;
        text.text = sceneData.characters[sceneData.characterIndex].descriptiveText;
        img.color = new Color(img.color.r, img.color.g, img.color.b, .5f);
    }

    IEnumerator FlyAway()
    {
        anim = displayObject.GetComponent<Animator>();
        anim.SetTrigger("Start");

        while (true)
        {
            displayObject.transform.position += displayObject.transform.forward * 5 * Time.deltaTime;
            displayObject.transform.position += displayObject.transform.up * 1 * Time.deltaTime;

            StartCoroutine(Repeat());

            yield return null;
        }

    }

    IEnumerator Repeat()
    {
        yield return new WaitForSeconds(1.5f);
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);
    }
}
