﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterData")]
public class CharacterData : ScriptableObject {
    public GameObject visualPrefab;
    public PlayerController prefab;
    public string descriptiveText;
    public Color uiShade;
}
