﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShaderCollection : MonoBehaviour
{

    class ShaderSet
    {
        public Shader[] shaders;

        public ShaderSet(Shader[] shaders)
        {
            this.shaders = shaders;
        }
    }


    private bool fetchAutomatically = true;
    private bool includeSelf = true;
    private bool useParent = false;
    [SerializeField]
    private List<Renderer> renderers = new List<Renderer>();
    private List<ShaderSet> prevShaders = new List<ShaderSet>();
    private Transform tf = null;

    public bool FetchAutomatically
    {
        get { return fetchAutomatically; }
        set { fetchAutomatically = value; }
    }

    public bool IncludeSelf
    {
        get { return includeSelf; }
        set { includeSelf = value; }
    }

    public bool UseParent
    {
        get { return useParent; }
        set { useParent = value; }
    }

    public List<Renderer> Renderers
    {
        get { return renderers; }
    }

    private void Awake()
    {
        tf = useParent ? transform.parent : transform;
    }

    private void Start()
    {
        if (renderers == null)
            renderers = new List<Renderer>();

        if (fetchAutomatically)
        {
            FetchShaders();
        }
    }

    public void UpdateShaders(Shader shader)
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            Material[] mats = renderers[i].materials;
            for (int x = 0; x < mats.Length; x++)
            {
                mats[x].shader = shader;
            }
            renderers[i].materials = mats;
        }
    }

    public void FetchShaders()
    {
        renderers = new List<Renderer>();
        if (tf.childCount > 0)
        {
            renderers.AddRange(tf.GetComponentsInChildren<Renderer>());
        }
        if (includeSelf)
        {
            Renderer renderer = tf.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderers.Add(renderer);
            }
        }

        int x = 0;
        while (x < renderers.Count)
        {
            if (!renderers[x].GetComponent<ParticleSystem>())
            {
                Material[] prevMaterials = renderers[x].sharedMaterials;
                List<Shader> s = new List<Shader>();
                for (int i = 0; i < prevMaterials.Length; i++)
                {
                    s.Add(prevMaterials[i].shader);
                }
                prevShaders.Add(new ShaderSet(s.ToArray()));
                x++;
            }
            else
            {
                renderers.Remove(renderers[x]);
            }
        }
    }

    public void RestoreShaders()
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            for (int x = 0; x < renderers[i].sharedMaterials.Length; x++)
            {
                renderers[i].sharedMaterials[x].shader = prevShaders[i].shaders[x];
            }
        }
    }

    public void SetShaderValue(string valueName, int value)
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            for (int x = 0; x < renderers[i].materials.Length; x++)
            {
                renderers[i].materials[x].SetInt(valueName, value);
            }
        }
    }

    public void SetShaderValue(string valueName, float value)
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            for (int x = 0; x < renderers[i].materials.Length; x++)
            {
                renderers[i].materials[x].SetFloat(valueName, value);
            }
        }
    }

    public void SetShaderValue(string valueName, Color value)
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            for (int x = 0; x < renderers[i].materials.Length; x++)
            {
                renderers[i].materials[x].SetColor(valueName, value);
            }
        }
    }

    public void SetShaderValue(string valueName, Vector4 value)
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            for (int x = 0; x < renderers[i].materials.Length; x++)
            {
                renderers[i].materials[x].SetVector(valueName, value);
            }
        }
    }

    public void SetShaderValue(string valueName, Texture value)
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            for (int x = 0; x < renderers[i].materials.Length; x++)
            {
                renderers[i].materials[x].SetTexture(valueName, value);
            }
        }
    }

    public void SetShaderValue(string valueName, Vector2 value)
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            for (int x = 0; x < renderers[i].materials.Length; x++)
            {
                renderers[i].materials[x].SetTextureOffset(valueName, value);
            }
        }
    }
}