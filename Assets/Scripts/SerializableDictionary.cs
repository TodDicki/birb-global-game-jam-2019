﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SerializableDictionary<K, V>  {
    
    public List<K> keys = new List<K>();
    public List<V> values = new List<V>();
    
    public V this[K key] { get { return values[GetIndexFromKey(key)]; } set { values[GetIndexFromKey(key)] = value; } }

    public void Add(K key, V value)
    {
        Remove(key);
        keys.Add(key);
        values.Add(value);
    }

    public void Clear()
    {
        keys.Clear();
        values.Clear();
    }

    public bool Contains(K key)
    {
        return keys.Contains(key);
    }
    
    public void Remove(K key)
    {
        if (keys.Contains(key))
        {
            int index = keys.IndexOf(key);
            keys.RemoveAt(index);
            values.RemoveAt(index);
        }
    }

    private int GetIndexFromKey(K key)
    {
        if (keys.Contains(key))
            return keys.IndexOf(key);
        return -1;
    }
}
