﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFollower : MonoBehaviour {

    [SerializeField]
    private Transform worldObject = null;
    [SerializeField]
    private float screenBuffer = 40f;
    private RectTransform tf = null;
    [SerializeField]
    private RectTransform bottomPoint;
    

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        //RotateToTarget();
        tf.position = GetDesiredPos();
    }

    void RotateToTarget()
    {
        Vector2 target = RectTransformUtility.WorldToScreenPoint(Camera.main, worldObject.position);
        float targetRotation = Mathf.Atan((target.y - bottomPoint.position.y) / (target.x - bottomPoint.position.x)) * Mathf.Rad2Deg;
        tf.eulerAngles = new Vector3(0, 0, targetRotation);
    }

    Vector2 GetDesiredPos()
    {
        Vector3 heading = worldObject.position - Camera.main.transform.position;
        float dot = Vector3.Dot(heading, Camera.main.transform.forward);
        Vector2 ret = RectTransformUtility.WorldToScreenPoint(Camera.main, worldObject.position);

        if (dot < 0)
        {
            ret.x = screenBuffer;
            ret.y = screenBuffer;
        }
        if (ret.x < screenBuffer)
            ret.x = screenBuffer;
        if (ret.y < screenBuffer)
            ret.y = screenBuffer;
        if (ret.x > Screen.width - screenBuffer)
            ret.x = Screen.width - screenBuffer;
        if (ret.y > Screen.height - screenBuffer)
            ret.y = Screen.height - screenBuffer;
        return ret;
    }
}
