﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class CreatureFlightPhysics : FreeFlightPhysics {

//    protected bool isFlapping = false;
//    public bool IsFlapping { get { return isFlapping; } }

//    private bool wingsHaveFlappedInDownPosition = false;
//    protected float currentFlapTime = 0.0f;

//    public void Execute(CreatureFlightPhysics controller)
//    { }

//    public CreatureFlightPhysics(Rigidbody rb) : base(rb) { }

//    public void DirectionalInput(float bank, float pitch, float sensitivity)
//    {
//        Quaternion desiredDirectionalInput = Quaternion.identity;
//        desiredDirectionalInput.eulerAngles = new Vector3(pitch, rigidbody.rotation.eulerAngles.y, bank);

//        rigidbody.MoveRotation(Quaternion.Lerp(rigidbody.rotation, desiredDirectionalInput, sensitivity * Time.deltaTime));

//    }

//    public void Flap(float minFlapTime, float regFlapTime, float flapStrength, float downBeatStrength, bool regFlap, bool quickFlap)
//    {
//        currentFlapTime += Time.deltaTime;
//        if (regFlap && WingsOpen())
//        {
//            if (!isFlapping || (quickFlap && currentFlapTime > minFlapTime))
//            {
//                isFlapping = true;
//                currentFlapTime = 0.0f;
//                rigidbody.AddForce(rigidbody.rotation * Vector3.up * flapStrength);
//            }

//            if (isFlapping)
//            {
//                if (currentFlapTime > regFlapTime * 0.9f && !wingsHaveFlappedInDownPosition)
//                {
//                    rigidbody.AddForce(rigidbody.rotation * Vector3.down * downBeatStrength);
//                    wingsHaveFlappedInDownPosition = true;
//                }
//                else if (currentFlapTime > regFlapTime)
//                {
//                    isFlapping = false;
//                    wingsHaveFlappedInDownPosition = false;
//                }
//            }
//        }
//    }

//    public void WingFold(float left, float right)
//    {
//        SetWingPosition(left, right);

//        float torqueSpeed = (rigidbody.velocity.magnitude) / 15.0f;
//        rigidbody.AddTorque(rigidbody.rotation * Vector3.forward * (right - left) * torqueSpeed);
//        rigidbody.angularDrag = left * right * 3.0f;

//        if (!WingsOpen())
//        {
//            Quaternion pitchRot = Quaternion.identity;
//            pitchRot.eulerAngles = new Vector3(AngleOfAttack, 0, 0);
//            pitchRot = rigidbody.rotation * pitchRot;
//            rigidbody.rotation = Quaternion.Lerp(rigidbody.rotation, pitchRot, torqueSpeed * Time.deltaTime);
//        }
//    }

//    public float GetAOAForZeroLCOF()
//    {
//        return 0.0f;
//    }
//}
