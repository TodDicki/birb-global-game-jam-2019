﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class FreeFlightPhysics  {

//    public bool liftEnabled = true;
//    public bool dragEnabled = true;
//    public bool gravityEnabled = true;

//    private float formDrag;
//    private float liftInducedDrag;

//    private float liftForce;
//    private float dragForce;
//    private Vector3 directionalLift;
//    private Vector3 directionalDrag;
//    private float liftCoefficient;
//    private float dragCoefficient;
//    private float angleOfAttack;

//    private float wingChord;
//    private float wingSpan;
//    private float wingArea;
//    private float aspectRatio;
//    private float weight;



//    protected float currentWingArea;
//    protected float leftWingExposure;
//    protected float rightWingExposure;

//    protected Rigidbody rigidbody;

//    public FreeFlightPhysics(Rigidbody rb)
//    {
//        rigidbody = rb;
//    }

//    public float FormDrag
//    {
//        get { return formDrag; }
//    }

//    public float LiftInducedDrag
//    {
//        get { return liftInducedDrag; }
//    }

//    public float Drag
//    {
//        get { return dragForce; }
//    }

//    public float List
//    {
//        get { return liftForce; }
//    }

//    public float LiftCoefficient
//    {
//        get { return liftCoefficient; }
//    }

//    public float DragCoefficient
//    {
//        get { return dragCoefficient; }
//    }

//    public float VerticalSpeed
//    {
//        get
//        {
//            return rigidbody.velocity.y;
//        }
//    }

//    public float Speed
//    {
//        get { return rigidbody.velocity.magnitude * 3.6f; }
//    }

//    public float AngleOfAttack
//    {
//        get { return angleOfAttack; }
//    }

//    public Vector3 Velocity
//    {
//        get { return rigidbody.velocity; }
//    }

//    public Vector3 Rotation
//    {
//        get { return rigidbody.rotation.eulerAngles; }
//    }

//    public float LeftWingExposure { get { return leftWingExposure; } }
//    public float RightWingExposure { get { return rightWingExposure; } }

//    public float WingSpan
//    {
//        get { return wingSpan * (leftWingExposure + rightWingExposure) / 2; }
//        set { wingSpan = value; }
//    }

//    public float WingChord
//    {
//        get { return wingChord * (leftWingExposure + rightWingExposure) / 2; }
//        set { wingChord = value; }
//    }

//    public float WingArea
//    {
//        get { return wingArea * (leftWingExposure + rightWingExposure) / 2; }
//        set { wingArea = value; }
//    }

//    public float AspectRatio
//    {
//        get { return aspectRatio; }
//        set { aspectRatio = value; }
//    }

//    public float Weight
//    {
//        get { return weight; }
//        set { weight = value; }
//    }

//    public FreeFlightPhysics()
//    {
//        SetWingPosition(1.0f, 1.0f);
//        wingSpan = 1.715f;
//        wingChord = .7f;
//        weight = 1.55f;
//        SetFromWingDimensions();
//    }

//    public void DoStandardPhysics()
//    {
//        if (rigidbody.isKinematic)
//            return;

//        rigidbody.useGravity = gravityEnabled;

//        rigidbody.rotation = GetBankedTurnRotation(rigidbody.rotation);

//        rigidbody.velocity = Vector3.Lerp(rigidbody.velocity, GetDirectionalVelocity(rigidbody.rotation, rigidbody.velocity), Time.deltaTime);
//       // Debug.Log(rigidbody.velocity);

//        angleOfAttack = GetAngleOfAttack(rigidbody.rotation, rigidbody.velocity);

//        if (rigidbody.velocity != Vector3.zero)
//        {
//            if (liftEnabled)
//            {
//                liftCoefficient = GetLiftCoefficent(angleOfAttack);

//                liftForce = GetLift(rigidbody.velocity.magnitude, 0, currentWingArea, liftCoefficient) * Time.deltaTime;
//                if (!float.IsNaN(liftForce))
//                {
//                    directionalLift = Quaternion.LookRotation(rigidbody.velocity) * Vector3.up;
//                    rigidbody.AddForce(directionalLift * liftForce);
//                }
//            }

//            if (dragEnabled)
//            {
//                dragCoefficient = GetDragCoefficent(angleOfAttack);

//                dragForce = GetDrag(rigidbody.velocity.magnitude, 0, currentWingArea, dragCoefficient, liftForce, AspectRatio) * Time.deltaTime;
//                if (!float.IsNaN(dragForce))
//                {
//                    directionalDrag = Quaternion.LookRotation(rigidbody.velocity) * Vector3.back;
//                    rigidbody.AddForce(directionalDrag * dragForce);
//                }
//            }
//        }
//        else
//        {
////rigidbody.velocity = rigidbody.transform.forward;
//        }
//    }

//    public float GetLift(float velocity, float pressure, float area, float liftCoff)
//    {
//        pressure = 1.225f;
//        float lift = velocity * velocity * pressure * area * liftCoff;
//        return lift;
//    }

//    public float GetDrag(float velocity, float pressure, float area, float dragCoff, float lift, float aspectR)
//    {
//        float VSEV = .9f;
//        pressure = 1.225f;

//        liftInducedDrag = (lift * lift) / (.5f * pressure * velocity * velocity * area * Mathf.PI * VSEV * aspectR);
//        formDrag = .5f * pressure * velocity * velocity * area * dragCoff;
//        return LiftInducedDrag + FormDrag;
//    }

//    public Quaternion GetStallRotation(Quaternion curRot, float velocity)
//    {
//        float pitchRotationSpeed = 10.0f / (velocity * velocity);
//        Quaternion pitchRot = Quaternion.LookRotation(Vector3.down);
//        Quaternion newRot = Quaternion.Lerp(curRot, pitchRot, Mathf.Abs(pitchRotationSpeed) * Time.deltaTime);
//        return newRot;
//    }

//    public Vector3 GetDirectionalVelocity(Quaternion currentRotation, Vector3 currentVelocity)
//    {
//        Vector3 vel = (currentRotation * Vector3.forward).normalized * currentVelocity.magnitude;
//        return vel;
//    }

//    public Quaternion GetBankedTurnRotation(Quaternion currentRotation)
//    {
//        float zRot = Mathf.Sin(currentRotation.eulerAngles.z * Mathf.Deg2Rad) * Mathf.Rad2Deg;
//        float prevX = currentRotation.eulerAngles.x;
//        Vector3 rot = new Vector3(0, -zRot * 0.8f, -zRot * 0.5f) * Time.deltaTime;

//        Quaternion angVel = Quaternion.identity;

//        angVel.eulerAngles = rot;
//        angVel *= currentRotation;
//        angVel.eulerAngles = new Vector3(prevX, angVel.eulerAngles.y, angVel.eulerAngles.z);

//        return angVel;
//    }

//    public float GetLiftCoefficent(float angleDegrees)
//    {
//        return 2 * Mathf.PI * angleDegrees * Mathf.Deg2Rad;
//    }

//    public float GetDragCoefficent(float angleDegrees)
//    {
//        return .0039f * angleDegrees * angleDegrees + 0.25f;
//    }

//    public float GetAngleOfAttack(Quaternion currentRotation, Vector3 currentVelocity)
//    {
//        Vector3 dirVel = (currentVelocity != Vector3.zero) ? Quaternion.LookRotation(currentVelocity) * Vector3.up : Vector3.up;

//        Vector3 forward = currentRotation * Vector3.forward;

//        return Mathf.Asin(Vector3.Dot(forward, dirVel)) * Mathf.Rad2Deg;
//    }

//    public void SetWingPosition(float cleftWingExposure, float cRightWingExposure)
//    {
//        leftWingExposure = (cleftWingExposure == 0.0f) ? 0.01f : cleftWingExposure;
//        rightWingExposure = (cRightWingExposure == 0.0f) ? 0.01f : cRightWingExposure;
//        currentWingArea = wingSpan * wingChord * (leftWingExposure + rightWingExposure) / 2;
//    }

//    public bool WingsOpen()
//    {
//        return currentWingArea == wingArea;
//    }

//    public void SetFromWingDimensions()
//    {
//        if (wingChord > 0 && wingSpan > 0)
//        {
//            wingArea = wingChord * wingSpan;
//            aspectRatio = wingSpan / wingChord;
//        }
//    }

//    public void SetWingDimensions()
//    {
//        if (aspectRatio > 0 && wingArea > 0)
//        {
//            wingSpan = Mathf.Sqrt(wingArea * aspectRatio);
//            wingChord = Mathf.Sqrt(wingArea / aspectRatio);
//        }
//    }

//}
