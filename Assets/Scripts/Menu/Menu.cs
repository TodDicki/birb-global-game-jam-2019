﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public static Menu menu = null;
    [SerializeField]
    private GameObject menuPanel;
    [SerializeField]
    private GameObject pausePanel;

    public bool isMenu = true;

    // Use this for initialization
    void Start()
    {
        if (menu == null)
        {
            menu = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        SceneManager.sceneLoaded += ((val1, val2) => {
            Controller c = FindObjectOfType<Controller>();
            c.RegisterButtonEvent(Controller.Button.Start, Controller.HeldState.Pressed, Pause);
            if(val1.buildIndex == 0)
                TurnOnMenu();
            Resume();
            pausePanel.SetActive(false);
        });
    }


    // Update is called once per frame
    void Update()
    {
        //if (!isMenu)
        //{
        //    if (Input.GetKeyDown(KeyCode.Escape))
        //    {
        //        pausePanel.SetActive(true);
        //        Cursor.visible = true;
        //        Cursor.lockState = CursorLockMode.None;
        //    }
        //}
    }

    private void Pause()
    {
        if (SceneManager.GetActiveScene().buildIndex != 0 && SceneManager.GetActiveScene().buildIndex != 1)
        {
            if (!isMenu)
            {

                pausePanel.SetActive(true);
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                Time.timeScale = 0;
            }
        }
    }

    public void SceneLoad(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void TurnOnMenu()
    {
        isMenu = true;
        menuPanel.SetActive(true);
        pausePanel.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void TurnOffMenu()
    {
        isMenu = false;
        menuPanel.SetActive(false);
        pausePanel.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Resume()
    {
        pausePanel.SetActive(false);
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
