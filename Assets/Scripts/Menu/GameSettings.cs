﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    [SerializeField]
    private Slider timeSlider;
    [SerializeField]
    private Text dayText;
    [SerializeField]
    private Toggle toggle;
    [SerializeField]
    private Toggle invertYToggle;
    [SerializeField]
    private Toggle invertXToggle;
    [SerializeField]
    public Light directionalLight;

    private Menu menu;

    public bool timerOn = true;

    public bool invertY = true;

    public bool invertX = true;

    // Use this for initialization
    void Start()
    {
        invertYToggle.onValueChanged.AddListener(SetYInversion);
        invertXToggle.onValueChanged.AddListener(SetXInversion);
        menu = GetComponent<Menu>();
        DayTextChanger();
    }


    public void DayTextChanger()
    {
        float degreesPerhour = 15.0f;
        int hour = (int)(timeSlider.value / degreesPerhour);

        float remaining = timeSlider.value / degreesPerhour;
        if(hour > 0)
            remaining = (timeSlider.value % ((hour * degreesPerhour))) / degreesPerhour;
        remaining = Mathf.Lerp(0, 59, remaining);
        hour += 6;
        bool pm = hour > 12;
        hour = hour % 13;
        if (pm)
            hour++;
        if (float.IsNaN(remaining))
            remaining = 0f;

        dayText.text = hour + " : " + ((int)remaining).ToString("00") + ((!pm) ? " AM" : " PM");// " AM";// timeSlider.value.ToString();

        TimeChanger();
    }

    public void TimeChanger()
    {
        if (directionalLight == null)
            directionalLight = FindObjectOfType<Light>();
        directionalLight.transform.eulerAngles = new Vector3(timeSlider.value, 0f, 0f);
    }

    public void TurnOffTimer()
    {
        if (toggle.isOn)
        {
            timerOn = true;
        }
        else
        {
            timerOn = false;
        }
    }

    public void SetYInversion(bool value)
    {
        invertY = value;
    }

    public void SetXInversion(bool value)
    {
        invertX = value;
    }
}
