﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCanvas : MonoBehaviour
{
    [SerializeField]
    private float timeLeft;

    [SerializeField]
    private Text time;
    [SerializeField]
    private Text weight;
    [SerializeField]
    public Text carryCapacity;
    [SerializeField]
    private GameObject gameOverPanel;
    [SerializeField]
    private Slider speedIndicator = null;
    [SerializeField]
    private Gradient speedGradient;
    [SerializeField]
    private Image speedImage;
    
    public Text weightGainText;
    public Text carryGainText;

    [SerializeField]
    private Image[] images = null;

    private Nest nest;
    private PlayerController player;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        nest = FindObjectOfType<Nest>();
        GameSettings gSet = FindObjectOfType<GameSettings>();
        if (!gSet.timerOn)
        {
            timeLeft = 0;
            //time.gameObject.SetActive(false);

            time.text = "∞";

        }
        //else
        {
            StartCoroutine(Timer());
        }

        SceneDataHolder sceneData = FindObjectOfType<SceneDataHolder>();
        for (int i = 0; i < images.Length; i++)
        {
            images[i].color = sceneData.characters[sceneData.characterIndex].uiShade;
        }

    }

    private void Update()
    {
        player = FindObjectOfType<PlayerController>();
        speedIndicator.value = player.SpeedPercent;
        speedImage.color = speedGradient.Evaluate(player.SpeedPercent);
    }

    void TickTime(float totalSeconds)
    {
        if (timeLeft != 0)
        {
            timeLeft -= Time.deltaTime;

            int minutes = Mathf.FloorToInt(totalSeconds / 60f);
            int seconds = Mathf.FloorToInt(totalSeconds % 60f);

            if (seconds == 60)
            {
                seconds = 0;
                minutes += 1;
            }

            time.text = minutes.ToString("0") + ":" + seconds.ToString("00");

            if (timeLeft < 1)
            {
                GameOver();
            }
            else
            {
                gameOverPanel.SetActive(false);
            }
        }
    }

    IEnumerator Timer()
    {
        while (true)
        {
            TickTime(timeLeft);
            weight.text = "Nest Weight: " + nest.NestWeight.ToString("");

            yield return null;
        }
    }

    void GameOver()
    {
        gameOverPanel.SetActive(true);
    }

    public void LoadScene(int index)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(index);
    }
}
