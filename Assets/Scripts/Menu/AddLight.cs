﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddLight : MonoBehaviour
{
    void Start()
    {
        GameSettings gSet = FindObjectOfType<GameSettings>();
        gSet.directionalLight = gameObject.GetComponent<Light>();
        gSet.TimeChanger();
    }
}
