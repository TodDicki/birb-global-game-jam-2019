﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashCollecter : MonoBehaviour
{
    public Transform holdPosition;
    public Pickup currentObject;

    [SerializeField]
    private float holdWeight = 15;
    [SerializeField]
    public float currentWeight;

    [HideInInspector] public PlayerCanvas playerCanvas;

    public bool canHoldMore = true;

    public float HoldWeight
    {
        get { return holdWeight; }
        set { holdWeight = value; }
    }

    public float CurrentWeight
    {
        get { return currentWeight; }
        set
        {
            currentWeight = value;
        }
    }

    // Use this for initialization
    void Start()
    {
        Controller c = FindObjectOfType<Controller>();
        c.RegisterButtonEvent(Controller.Button.Y, Controller.HeldState.Pressed, Drop);
        playerCanvas = FindObjectOfType<PlayerCanvas>();
        playerCanvas.carryCapacity.text = "Carry Capacity: " + HoldWeight.ToString("0");
    }

    void Drop()
    {
        if (currentObject != null)
        {

            currentObject.transform.parent = null;
            currentObject.GetComponent<Rigidbody>().isKinematic = false;
            currentObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            currentObject.GetComponent<Rigidbody>().useGravity = true;
            StartCoroutine(DropItem());
        }
    }
    IEnumerator DropItem()
    {
        yield return new WaitForSeconds(0.75f);
        currentObject.enabled = true;
        currentWeight -= currentObject.Weight;
        currentObject = null;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (currentObject != null)
        {
            Nest n = other.GetComponent<Nest>();
            if (n == null)
                n = other.GetComponentInParent<Nest>();
            if (n != null)
            {

                currentObject.transform.parent = n.transform;
                currentObject.GetComponent<Rigidbody>().isKinematic = true;
                currentObject.GetComponent<Rigidbody>().freezeRotation = true;
                //currentObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                currentObject.GetComponent<Rigidbody>().useGravity = false;
                currentObject.enabled = false;
                currentWeight -= currentObject.Weight;
                n.NestWeight += currentObject.Weight;
                n.UpdateWeight(this);
                for (int i = 0; i < currentObject.bc.Length; i++)
                {
                    currentObject.bc[i].isTrigger = false;
                }
                Destroy(currentObject);
            }
        }

        if (currentObject == null)
        {
            currentObject = other.GetComponent<Pickup>();
            if (currentObject == null)
                currentObject = other.GetComponentInParent<Pickup>();
            Debug.Log(currentObject);
            if (currentObject != null)
            {
                if (currentObject.Weight > holdWeight)
                {
                    currentObject = null;
                    return;
                }


                currentObject.transform.parent = transform;
                currentObject.transform.localPosition = Vector3.zero;
                Bounds b = new Bounds();
                b.Encapsulate(currentObject.GetComponent<Collider>().bounds);
                currentObject.transform.localPosition = new Vector3(0, -b.extents.y - .1f, 0);

                currentObject.GetComponent<Rigidbody>().isKinematic = true;
                currentObject.GetComponent<Rigidbody>().freezeRotation = true;
                currentObject.GetComponent<Rigidbody>().useGravity = false;
                currentObject.enabled = false;
                currentWeight += currentObject.Weight;
                for (int i = 0; i < currentObject.bc.Length; i++)
                {
                    currentObject.bc[i].isTrigger = true;
                }
            }
        }
    }
}
