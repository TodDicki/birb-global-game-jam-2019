﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nest : MonoBehaviour
{

    [SerializeField]
    private float weightGain = 50f;
    [SerializeField]
    private float carryGain = 25f;

    private float nestWeight = 0f;
    public float NestWeight
    {
        get { return nestWeight; }
        set
        {
            if (player == null)
                player = FindObjectOfType<TrashCollecter>();
            float dif = value - nestWeight;
            nestWeight = value;
            StartCoroutine(TextDisplay(player.playerCanvas.weightGainText, "+" + dif.ToString(), 3f));
        }
    }

    [SerializeField]
    public float addWeightPoint = 50;

    private TrashCollecter player;


    IEnumerator TextDisplay(Text t, string text, float duration)
    {
        t.gameObject.SetActive(true);
        t.text = text;
        yield return new WaitForSeconds(duration);
        t.gameObject.SetActive(false);
    }

    public void UpdateWeight(TrashCollecter p)
    {
        player = p;
        if (NestWeight >= addWeightPoint)
        {
            StartCoroutine(TextDisplay(player.playerCanvas.carryGainText, "+" + carryGain.ToString(), 3f));
            player.HoldWeight += carryGain;
            addWeightPoint += weightGain;
            UpdateCarryCapacityUi();
        }
    }

    void UpdateCarryCapacityUi()
    {
        player.playerCanvas.carryCapacity.text = "Carry Capacity: " + player.HoldWeight.ToString("0");
    }

    void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.tag == "Player")
        //{
        //    player = other.gameObject.GetComponent<TrashCollecter>();
        //    UpdateWeight();

        //    if (player.currentObject != null)
        //    {
        //        player.currentObject.canBePickedUp = false;
        //        player.currentObject = null;
        //        player = null;
        //    }
        //}
    }
}
