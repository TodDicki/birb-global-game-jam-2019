﻿
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Transform target = null;
    [SerializeField]
    private float minBackDistance = -2f;
    [SerializeField]
    private float maxBackDistance = -15f;
    [SerializeField]
    private float minUpDistance = .8f;
    [SerializeField]
    private float maxUpDistance = 0.8f;
    [SerializeField]
    private float lag = 4f;
    [SerializeField]
    private float rotationLag = 4f;

    [SerializeField]
    private float flareCamHeight = 2f;
    [SerializeField]
    private float flareCamDist = -3f;
    [SerializeField]
    private float flareCamAngle = 50f;
    [SerializeField]
    private float flareDuration = 1.0f;
    [SerializeField]
    private float flareLag = 0.3f;

    private float flareLookTimer = 0.0f;
    private PlayerController player;

    private void Start()
    {
        player = target.GetComponent<PlayerController>();
    }

    private void LateUpdate()
    {
        float backDistance = Mathf.Lerp(minBackDistance, maxBackDistance, player.SpeedPercent);
        float upDistance = Mathf.Lerp(minUpDistance, maxUpDistance, player.SpeedPercent);
        if (player.isGrounded())
        {
            backDistance = minBackDistance;// Mathf.Lerp(minBackDistance, maxBackDistance, player.SpeedPercent);
            upDistance = minUpDistance;// Mathf.Lerp(minUpDistance, maxUpDistance, player.SpeedPercent);
        }
           // Debug.Log(upDistance);
            // = Mathf.Lerp(maxRotationLag, minRotationLag, player.SpeedPercent);
            //if (player.InputFlaring || flareLookTimer > 0f)
            //{
            //    FlareLook();
            //}
            //else
            {
                transform.position = Vector3.Lerp(transform.position, target.position + target.forward * backDistance + target.up * upDistance, 1);// 0.7f);//lag * Time.deltaTime);//Vector3.Lerp(transform.position, target.position + target.forward * backDistance + target.up * upDistance, lag * Time.deltaTime);

                transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, rotationLag * Time.deltaTime);
            }
        //Vector3 lookAtPoint = target.position + target.transform.forward;
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(target.position - lookAtPoint), rotationLag * Time.deltaTime);
    }

    public void SetTarget(Transform targ)
    {
        target = targ;
    }

    void FlareLook()
    {
        if (!player.isFlying())
        {
            flareLookTimer = -1;
            return;
        }

        Vector3 flareLookPosition = new Vector3(0f, flareCamHeight, flareCamDist);

        float currentPitch = Mathf.DeltaAngle(target.eulerAngles.x, 0);
        float desiredPitch = Mathf.Abs(flareCamAngle + currentPitch);

        flareLookTimer = player.InputFlaring ? flareDuration : flareLookTimer - Time.deltaTime;

        desiredPitch = desiredPitch * flareLookTimer / flareDuration;

        Quaternion dir = Quaternion.AngleAxis(desiredPitch, Vector3.right);

        dir = target.rotation * dir;

        transform.position = Vector3.Lerp(transform.position, target.TransformPoint(flareLookPosition), flareLag * Time.deltaTime);
       // transform.rotation = Quaternion.Lerp(transform.rotation, dir, rotationLag * Time.deltaTime);

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red; player = target.GetComponent<PlayerController>();
        float backDistance = Mathf.Lerp(minBackDistance, maxBackDistance, player.SpeedPercent);
        float upDistance = Mathf.Lerp(minUpDistance, maxUpDistance, player.SpeedPercent);
        Gizmos.DrawSphere(target.position + target.forward * backDistance + target.up * upDistance, 1f);
    }
}