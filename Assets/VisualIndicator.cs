﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualIndicator : MonoBehaviour {

    [SerializeField]
    private Shader outlineShader = null;
    [SerializeField]
    private UnityEngine.UI.Text displayText = null;
    Pickup currentPickup = null;

	// Update is called once per frame
	void Update () {
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, 3.5f, transform.forward);

        Pickup p = null;
        bool exit = false;
        for (int i = 0; i < hits.Length; i++)
        {
            if (!exit && hits[i].collider.GetComponent<Pickup>())
            {
                p = hits[i].collider.GetComponent<Pickup>();
                exit = true;
            }
        }
        if (p != null)
        {
            if (currentPickup != null)
                currentPickup.GetComponent<ShaderCollection>().RestoreShaders();
            currentPickup = p;
            currentPickup.GetComponent<ShaderCollection>().UpdateShaders(outlineShader);
            currentPickup.GetComponent<ShaderCollection>().SetShaderValue("_Tint", Color.cyan);// (outlineShader);
        }

        if (currentPickup != null)
        {
            displayText.gameObject.SetActive(true);
            displayText.text = currentPickup.Weight.ToString();
            displayText.transform.position = GetDesiredPos(currentPickup.transform.position);
        }
        else
        {
            displayText.gameObject.SetActive(false);
        }
	}

    Vector2 GetDesiredPos(Vector3 worldPos)
    {
        Vector3 heading = worldPos - Camera.main.transform.position;
        float dot = Vector3.Dot(heading, Camera.main.transform.forward);
        Vector2 ret = RectTransformUtility.WorldToScreenPoint(Camera.main, worldPos);

        
        return ret;
    }
}
