﻿using System.Collections;
using UnityEngine;

public class CameraFit : MonoBehaviour
{

    private Camera cam;

    [SerializeField]
    private Transform target = null;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    private void OnEnable()
    {
        cam = GetComponent<Camera>();
        Bounds b = new Bounds();
        foreach (var item in target.GetComponentsInChildren<MeshRenderer>())
        {
            b.Encapsulate(item.bounds);
        }
        foreach (var item in target.GetComponentsInChildren<Collider>())
        {
            b.Encapsulate(item.bounds);
        }

        ZoomFit(b, true);
    }

    IEnumerator ZoomPosition(Vector3 originalPos, Vector3 toPos, float speed = 2f)     {         float t = 0;         while (t < 1)
        {             t += Time.deltaTime * speed;             transform.position = Vector3.Lerp(originalPos, toPos, t);             yield return null;         }     }      public void ZoomFit(Bounds bounds, bool overTime = false)     {         transform.LookAt(bounds.center);          Vector3 originalPos = transform.position;         transform.position = bounds.center;         while (!CanViewEntireBounds(bounds))
        {             transform.position -= transform.forward;         }
        if (overTime)
        {             Vector3 newPos = transform.position;             transform.position = originalPos;             if (gameObject.activeSelf)
            {                 StopAllCoroutines();                 StartCoroutine(ZoomPosition(originalPos, newPos));             }         }     }
    private bool CanViewEntireBounds(Bounds b)     {         Vector3[] boundPoints = GetCorners(b);          for (int i = 0; i < boundPoints.Length; i++)
        {             Vector3 viewportPoint = cam.WorldToViewportPoint(boundPoints[i]);             if (viewportPoint.z <= 0 || !cam.rect.Contains(viewportPoint))                 return false;         }          return true;      }      private Vector3[] GetCorners(Bounds bounds)     {         Vector3[] boundPoints = new Vector3[8];          boundPoints[0] = bounds.min;         boundPoints[1] = bounds.max;         boundPoints[2] = new Vector3(boundPoints[0].x, boundPoints[0].y, boundPoints[1].z);         boundPoints[3] = new Vector3(boundPoints[0].x, boundPoints[1].y, boundPoints[0].z);         boundPoints[4] = new Vector3(boundPoints[1].x, boundPoints[0].y, boundPoints[0].z);         boundPoints[5] = new Vector3(boundPoints[0].x, boundPoints[1].y, boundPoints[1].z);         boundPoints[6] = new Vector3(boundPoints[1].x, boundPoints[0].y, boundPoints[1].z);         boundPoints[7] = new Vector3(boundPoints[1].x, boundPoints[1].y, boundPoints[0].z);          return boundPoints;     } }